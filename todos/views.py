from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from todos.models import Todo
from django.urls import reverse_lazy

# Create your views here.


class TodoCreateView(LoginRequiredMixin, CreateView):
    model = Todo
    template_name = "todos/create.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "category",
        "description",
        "user",
    ]
    def get_success_url(self) -> str:
        return reverse_lazy("show_category", args=[self.object.category.id])


class TodoListView(LoginRequiredMixin, ListView):
    model = Todo
    template_name = "todos/list.html"

    def get_queryset(self):
        return Todo.objects.filter(user=self.request.user)


class TodoUpdateView(LoginRequiredMixin, UpdateView):
    model = Todo
    fields = ["is_completed"]
    success_url = "/todos/mine/"


class TodoDeleteView(LoginRequiredMixin, DeleteView):
    model = Todo
    fields = ["delete"]
    success_url = "/todos/mine/"


class TodoDetailView(LoginRequiredMixin, DetailView):
    model = Todo
    fields = ["description"]
    template_name = "todos/detail.html"