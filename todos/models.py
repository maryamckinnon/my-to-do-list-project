from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.


class Todo(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(null=False, blank=False)
    due_date = models.DateTimeField(auto_now=False, null=False)
    is_completed = models.BooleanField(default=False)
    category = models.ForeignKey(
        "categories.Category", related_name="todos", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="todos",
        on_delete=models.SET_NULL,
    )
    description = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.name