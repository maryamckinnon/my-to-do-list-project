from django.urls import path
from todos.views import (
    TodoCreateView,
    TodoListView,
    TodoUpdateView,
    TodoDeleteView,
    TodoDetailView,
)

urlpatterns = [
    path("create/", TodoCreateView.as_view(), name="create_todo"),
    path("mine/", TodoListView.as_view(), name="todo_list"),
    path("<int:pk>/complete/", TodoUpdateView.as_view(), name="complete_todo"),
    path("<int:pk>/delete/", TodoDeleteView.as_view(), name="delete_todo"),
    path("<int:pk>/detail", TodoDetailView.as_view(), name="todo_detail"),
]
