from categories.models import Category
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


# Create your views here.


class CategoryListView(LoginRequiredMixin, ListView):
    model = Category
    template_name = "categories/list.html"
    success_url = "home"

    def get_queryset(self):
        return Category.objects.filter(members=self.request.user)


class CategoryDetailView(LoginRequiredMixin, DetailView):
    model = Category
    template_name = "categories/detail.html"


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = Category
    template_name = "categories/create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_category", args=[self.object.id])