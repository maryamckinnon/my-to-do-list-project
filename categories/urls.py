from categories.views import (
    CategoryListView,
    CategoryDetailView,
    CategoryCreateView,
)
from django.urls import path

urlpatterns = [
    path("", CategoryListView.as_view(), name="category_list"),
    path("<int:pk>/", CategoryDetailView.as_view(), name="show_category"),
    path("create/", CategoryCreateView.as_view(), name="create_category"),
]
